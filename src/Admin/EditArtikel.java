package Admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import User.Conf;

/**
 * Servlet implementation class EditArtikel
 */

public class EditArtikel extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditArtikel() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		if (session.getAttribute("is_login") != null)
		{
			try
			{
				Integer ID = Integer.parseInt(request.getParameter("id"));
				Class.forName(Conf.DRIVER);
				Connection conn = DriverManager.getConnection(Conf.DB_URL,Conf.USER,Conf.PASS);
				Statement S = conn.createStatement();
				ResultSet RS = S.executeQuery("select * from artikel where id="+ID);
				if (RS.next()){
					out.println("<form action='"+Conf.WEB+"/admin/edit?id="+RS.getString("id")+"' method='POST'>");
					out.println("Judul : <input type='text' name='judul' value='"+RS.getString("judul")+"'><br>");
					out.println("Isi : <textarea name='isi'>"+RS.getString("isi")+"</textarea><br>");
					out.println("<input type='submit' value='submit'><br>");
					out.println("</form>");
				}else
				{
					out.println("Tidak ada data");
				}
				RS.close();
				S.close();
				conn.close();
			}catch(Exception e){out.println("<h1> 500 oops error :v</h1>");}
			out.println("<a href='"+Conf.WEB+"/admin'> Kembali </a>");
		}else
		{
			response.sendRedirect(Conf.WEB+"/admin/login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if (session.getAttribute("is_login") != null)
		{
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			try
			{
				String judul = request.getParameter("judul");
				String isi = request.getParameter("isi");
				Class.forName(Conf.DRIVER).newInstance();
				Connection conn = DriverManager.getConnection(Conf.DB_URL,Conf.USER,Conf.PASS);
				Statement S = conn.createStatement();
				String sql = "UPDATE artikel SET judul='"+judul+"', isi='"+isi+"' where id="+request.getParameter("id");
				S.executeUpdate(sql);
				S.close();
				conn.close();
				response.sendRedirect(Conf.WEB+"/admin");
			}catch (Exception e){out.println("<h1>500 Oops Error</h1>");}
		}else
		{
			response.sendRedirect(Conf.WEB+"/admin/login");
		}
	}

}
