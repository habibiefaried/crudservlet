package Admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import User.Conf;

/**
 * Servlet implementation class AddArtikel
 */

public class AddArtikel extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddArtikel() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if (session.getAttribute("is_login") != null)
		{
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("<form action='"+Conf.WEB+"/admin/add' method='POST'>");
			out.println("Judul : <input type='text' name='judul'><br>");
			out.println("Isi : <textarea name='isi'></textarea><br>");
			out.println("<input type='submit' value='submit'><br>");
			out.println("</form>");
		}else
		{
			response.sendRedirect(Conf.WEB+"/admin/login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if (session.getAttribute("is_login") != null)
		{
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			try
			{
				String judul = request.getParameter("judul");
				String isi = request.getParameter("isi");
				Class.forName(Conf.DRIVER);
				Connection conn = DriverManager.getConnection(Conf.DB_URL,Conf.USER,Conf.PASS);
				Statement S = conn.createStatement();
				S.execute("insert into artikel (judul,isi) values ('"+judul+"','"+isi+"')");
				S.close();
				conn.close();
				response.sendRedirect(Conf.WEB+"/admin");
			}catch (Exception e){out.println("<h1>500 oops error </h1>");}
		}else
		{
			response.sendRedirect(Conf.WEB+"/admin/login");
		}
	}

}
