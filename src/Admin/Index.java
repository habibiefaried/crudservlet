package Admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import User.Conf;

/**
 * Servlet implementation class Index
 */

public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if (session.getAttribute("is_login") != null)
		{
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			out.println("Hello admin!");
			out.println("<a href='"+Conf.WEB+"/admin/logout'> Logout </a><br>");
			try
			{
				out.println("<h2>List berita</h2><br>");
				out.println("<h3><a href='"+Conf.WEB+"/admin/add'> Tambah Artikel </a>");
				out.println("<table border = 0>");
				out.println("<tr>");
				out.println("<th>No</th>");
				out.println("<th>Judul</th>");
				out.println("<th>Isi</th>");
				out.println("<th>Aksi</th>");
				out.println("</tr>");
				Class.forName(Conf.DRIVER);
				Connection conn = DriverManager.getConnection(Conf.DB_URL,Conf.USER,Conf.PASS);
				Statement S = conn.createStatement();
				ResultSet RS = S.executeQuery("select * from artikel");
				int i = 0;
				while (RS.next())
				{
					i++;
					String judul = RS.getString("judul");
					String isi = RS.getString("isi");
					int ID = RS.getInt("id");
					out.println("<tr>");
					out.println("<td>"+i+"</td>");
					out.println("<td>"+judul+"</td>");
					out.println("<td>"+isi+"</td>");
					out.println("<td><a href="+Conf.WEB+"/admin/edit?id="+ID+"> Edit </a>");
					out.println("<a href="+Conf.WEB+"/admin/delete?id="+ID+"> Delete </a></td>");
					out.println("</tr>");
				}
				out.println("</table>");
				RS.close();
				S.close();
				conn.close();
			}catch(Exception e){out.println("<h1>500 oops error :v</h1>");}
		}else
		{
			response.sendRedirect(Conf.WEB+"/admin/login");
		}
	}
}
