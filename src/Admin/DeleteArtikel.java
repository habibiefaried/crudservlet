package Admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import User.Conf;

/**
 * Servlet implementation class DeleteArtikel
 */

public class DeleteArtikel extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteArtikel() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		if (session.getAttribute("is_login") != null)
		{
			try
			{
				Class.forName(Conf.DRIVER);
				Connection conn = DriverManager.getConnection(Conf.DB_URL,Conf.USER,Conf.PASS);
				Statement S = conn.createStatement();
				S.execute("delete from artikel where id="+request.getParameter("id"));
				S.close();
				conn.close();
				response.sendRedirect(Conf.WEB+"/admin");
			}catch (Exception e){out.println("<h1>500 oops error </h>");}
		}else
		{
			response.sendRedirect(Conf.WEB+"/admin/login");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
