package Admin;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import User.Conf;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<h1>Login Area</h1><br>");
		out.println("<form action='"+Conf.WEB+"/admin/login' method='post'>");
		out.println("Username : <input type='text' name='username'><br>");
		out.println("Password : <input type='password' name='password'><br>");
		out.println("<input type='submit' value='submit'>");
		out.println("</form>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String user = request.getParameter("username");
		String pass = request.getParameter("password");
		if ((user.equals("test")) && (pass.equals("test")))
		{
			HttpSession session = request.getSession();
			session.setAttribute("is_login", true);
			response.sendRedirect(Conf.WEB+"/admin");
		}
		else
		{
			response.sendRedirect(Conf.WEB+"/admin/login");
		}
	}

}
