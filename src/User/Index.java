package User;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Index
 */
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		//settingan database
	    
		try
		{
			out.println("<h2>Silahkan pilih berita</h2><br>");
			out.println("Jika anda admin, silahkan klik <a href='"+Conf.WEB+"/admin'> disini </a>");
			out.println("<table border = 0>");
			out.println("<tr>");
			out.println("<th>No</th>");
			out.println("<th>Judul</th>");
			out.println("</tr>");
			Class.forName(Conf.DRIVER);
			Connection conn = DriverManager.getConnection(Conf.DB_URL,Conf.USER,Conf.PASS);
			Statement S = conn.createStatement();
			ResultSet RS = S.executeQuery("select * from artikel");
			int i = 0;
			while (RS.next())
			{
				i++;
				String judul = RS.getString("judul");
				int ID = RS.getInt("id");
				out.println("<tr>");
				out.println("<td>"+i+"</td>");
				out.println("<td><a href="+Conf.WEB+"/home/lihat?id="+ID+">"+judul+"</a></td>");
				out.println("</tr>");
			}
			out.println("</table>");
			RS.close();
			S.close();
			conn.close();
		}catch(Exception e){out.println("<h1>500 oops error :v</h1>");}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
