package User;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LihatArtikel
 */

public class LihatArtikel extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LihatArtikel() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		try
		{
			Integer ID = Integer.parseInt(request.getParameter("id"));
			Class.forName(Conf.DRIVER);
			Connection conn = DriverManager.getConnection(Conf.DB_URL,Conf.USER,Conf.PASS);
			Statement S = conn.createStatement();
			ResultSet RS = S.executeQuery("select * from artikel where id="+ID);
			if (RS.next()){
			out.println("<h1>"+RS.getString("judul")+"</h1><br>");
			out.println(RS.getString("isi")+"<br>");
			}else
			{
				out.println("Tidak ada data");
			}
			RS.close();
			S.close();
			conn.close();
		}catch(Exception e){out.println("<h1> 500 oops error :v</h1>");}
		out.println("<a href='"+Conf.WEB+"/home'> Home </a>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
